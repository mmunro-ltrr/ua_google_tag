; ------------------------------------------------------------------------------
; UA Google Tag Makefile
;
; Downloads contrib module and library dependencies for UA Google Tag
; component.
; ------------------------------------------------------------------------------

core = 7.x
api = 2

; Set default contrib destination
defaults[projects][subdir] = contrib

; ------------------------------------------------------------------------------
; Contrib modules
; ------------------------------------------------------------------------------

projects[google_tag][version] = 1.4
; Fixes W3C HTML validation issue.
; @see https://jira.arizona.edu/browse/UADIGITAL-1990
; @see https://www.drupal.org/project/google_tag/issues/3009268
projects[google_tag][patch][3009268] = https://www.drupal.org/files/issues/2018-10-25/google_tag-remove_iframe_content_title-30092682-D7.patch
